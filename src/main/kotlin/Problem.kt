class Problem (val title:String,val statment:String,val entrance:String,val exit:String,val publicInput:String,
               val publicOutput:String,val privateInput:String,val privateOutput:String
){
    var solved = false
    var trys = mutableListOf<String>()
    var counterTrys = 0
    fun printProblem(n:Int){
        println("${colorGroc}Programa ${n+1}$colorReset: ${this.title}")
        println()
        println(this.exit)
        println(this.statment)
        println(this.entrance)
        println("${colorVerd}Exemple:$colorReset")
        println("     ${colorGroc}Input:$colorReset ${this.publicInput}")
        println("     ${colorGroc}Output:$colorReset ${this.publicOutput}")
        println()
        println("     ${colorGroc}Input:$colorReset ${this.privateInput}")
        println("     ${colorGris}Output:$colorReset ¿?")
        println()
    }

    fun resolvedProblem(){
        do{
            counterTrys++
            print("Escribe la respuesta:")
            val solution = scan.next()

            if(solution != this.privateOutput) {
                println("Es incorrecto")
                this.stats()
            }
            else{
                println()
                println("Es correcto")
                this.solved = true
                this.trys.add(solution)
                this.stats()
            }


        }while (!this.solved)
    }

    fun stats(){
        println()
        println("Tus intentos: $counterTrys")
        for(i in this.trys.indices) println("   - ${this.trys[i]}")
        if(!this.solved)println("${colorRed}no resuelto$colorReset")
        else  println("${colorVerd}resuelto$colorReset ")
        println()
    }

}