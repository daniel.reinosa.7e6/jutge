import com.sun.org.apache.xpath.internal.operations.Bool
import java.util.*
val scan = Scanner(System.`in`)
const val colorGris = "\u001b[37m"
const val colorGroc = "\u001b[33m"
const val colorVerd = "\u001b[32m"
const val colorRed = "\u001b[31m"
const val colorReset = "\u001b[0m"
const val colorBlue = "\u001b[34m"
fun main() {
    var gamePosition = 0
    var resolvedQuestions = 0
    var finish = true
    val scanner = Scanner(System.`in`)
    println()
    println()
    println("\n" +
            "                  ░░░░░██╗██╗░░░██╗████████╗░██████╗░███████╗\n" +
            "                  ░░░░░██║██║░░░██║╚══██╔══╝██╔════╝░██╔════╝\n" +
            "                  ░░░░░██║██║░░░██║░░░██║░░░██║░░██╗░█████╗░░\n" +
            "                  ██╗░░██║██║░░░██║░░░██║░░░██║░░╚██╗██╔══╝░░\n" +
            "                  ╚█████╔╝╚██████╔╝░░░██║░░░╚██████╔╝███████╗\n" +
            "                  ░╚════╝░░╚═════╝░░░░╚═╝░░░░╚═════╝░╚══════╝")
    println()
    println()
    do{
        for (i in problems.indices) {
            val problemPosition = problems[i]
            problemPosition.printProblem(i)
            println("     ${colorBlue}Enserio quieres resolverlo?$colorReset\n" +
                        "${colorVerd}Y$colorReset/${colorRed}N$colorReset\n")
            val userDecide = scan.next().uppercase()
            if (userDecide == "Y") {
                resolvedQuestions++
                gamePosition++
                problemPosition.resolvedProblem()
            } else if (userDecide == "N"){
                finish = false
                val text = ""
                println(text)
            }
        }
    }
    while (finish)

}

