val problems = listOf(
    Problem(
        "La puerta inaccesible: ¿eres capaz de hackearla?",
        "Nos disponemos a llamar al receptor para que nos abran la puerta de la mansion, recuerda que no podemos" +
                " llamar la atencion,\n" +
                "necesitamos dar con la contraseña correcta antes del tercer intento, nos hemos dado cuenta que el dispositivo" +
                " es de fujitsu y que tiene un BUG,\n" +
                "el siguiente BUG consiste en poner un numero de tres digitos la resta a este del numero que te devuelva" +
                " serà la solución",
        "Indica un numero de tres caracteres",
        "",
        "234",
        "202",
        "342",
        "310"
    ),
    Problem(
        "",
        "Hemos llegado al garage sin problemas, vamos a ojear el ordenador a ver si encontramos algo....\n" +
                "No hay mucha cosa pero si tenemos acceso a la red, la contrasenya no puede ser muy dificil,\npor suerte hemos traido el portable de Jhon the ripper" +
                "con este programa solo necesitaremos ver el patron por el que los numeros se multiplican y sabremos la contraseña.",
        "",
        "              ______--------___\n" +
                "             /|             / |\n" +
                "  o___________|_\\__________/__|\n" +
                " ]|___     |  |=   ||  =|___  |\"\n" +
                " //   \\\\    |  |____||_///   \\\\|\"\n" +
                "|  X  |\\--------------/|  X  |\\\"\n" +
                " \\___/ 1924 Studebaker  \\___/",
        "304",
        "608",
        "209",
        "418"
    ),
    Problem(
        "Som iguals?",
        "Estamos en el hall, tenemos que encontrar el despacho de Mr.Smith antes de que alguien se entere,tenemos" +
                " que adivinar el numero correcto de la columna",
        "",
        "",
        "2",
        "2",
        "6",
        "6",
    ),
    Problem(
        "La caja",
        "La contraseña de la caja esta oculta en la siguiente secuencia de numeros pero varia dependiendo del dia,\n" +
                "intenta adivinar el numero de division de estos numeros para poder acceder a la caja fuerte sin que suenen\n" +
                "las alarmas, ¡CUIDADO!, si nos pillan estamos acabados.",
        "",
        "",
        "540",
        "225",
        "200",
        "83.33",
    ),
    Problem(
        "Salida",
        "Estas saliendo de la mansion, mision cumplida pero solo falta, la ultima prueba, has de quemar esa casa," +
                "mirando las soluciones indica que valor le tienes que dar al mando para explotar la mansión.",
        "Boolean",
        "                           (   )\n" +
                "                          (    )\n" +
                "                           (    )\n" +
                "                          (    )\n" +
                "                            )  )\n" +
                "                           (  (                  /\\\n" +
                "                            (_)                 /  \\  /\\\n" +
                "                    ________[_]________      /\\/    \\/  \\\n" +
                "           /\\      /\\        ______    \\    /   /\\/\\  /\\/\\\n" +
                "          /  \\    //_\\       \\    /\\    \\  /\\/\\/    \\/    \\\n" +
                "   /\\    / /\\/\\  //___\\       \\__/  \\    \\/\n" +
                "  /  \\  /\\/    \\//_____\\       \\ |[]|     \\\n" +
                " /\\/\\/\\/       //_______\\       \\|__|      \\\n" +
                "/      \\      /XXXXXXXXXX\\                  \\\n" +
                "        \\    /_I_II  I__I_\\__________________\\\n" +
                "               I_I|  I__I_____[]_|_[]_____I\n" +
                "               I_II  I__I_____[]_|_[]_____I\n" +
                "               I II__I  I     XXXXXXX     I\n" +
                "            ~~~~~\"   \"~~~~~~~~~~~~~~~~~~~~~~~~",
        "true",
        "false",
        "true",
        "false",
    ))